Model Images
============
Add image admin functional to model

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).


Add

```
"repositories": [{
        "type": "git",
        "url": "file://D:\\work\\web\\xampp\\htdocs\\research\\common\\runtime\\tmp-extensions/yii2-modelimages"
    }
],
```

```
"k.kurill/yii2-modelimages": "*"
```

to the require section of your `composer.json` file.


Использование
-----

Миграции:
```
yii migrate/up --migrationPath=@kurill/modelimages/migrations


Используется модуль https://github.com/CostaRico/yii2-images:

Установка модуля:
```php
'modules' => [
    'yii2images' => [
        'class' => 'rico\yii2images\Module',
        //be sure, that permissions ok 
        //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
        'imagesStorePath' => '@frontend/web/images/store', //path to origin images
        'imagesCachePath' => '@frontend/web/images/cache', //path to resized copies
        'graphicsLibrary' => 'GD', //but really its better to use 'Imagick' 
        'placeHolderPath' => '@frontend/images/placeHolder.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
    ],
],
```
Behaviour Переопределен, поэтому подклюаетсяя по другому. В остальном всё также.

Модель которой нужны картинки
------
to your model (be sure that your model has "id" property)
```php
    public function behaviors(){
        return [
            'image' => [
                'class' => 'kurill\modelimages\behaviors\ImageBehave',
                'roles' => [
                    'mainImage' => [   
                        'label'=>'Main Image',
                        'maxCount'=>1, 
                        'aspectRatio'=>10/9,
                        'minWidth' => '400',
                        'minHeight' => '400',
                        'fileTypes' => 'jpg,jpeg,gif,png',
                        'maxFileSize' => 10485760, // 10mb],
                    ],
                ]
            ]
        ];
    }
```

Контроллер
----------

```php
public function actions()
    {
        return [
            'cropImage' => [
                'class' => \demi\image\CropImageAction::className(),
                'modelClass' => \kurill\modelimages\models\Image::className(),
                'redirectUrl' => function ($model) {
                        /* @var $model Post */
                        // triggered on !Yii::$app->request->isAjax, else will be returned JSON: {status: "success"}
                        return ['update', 'id' => $model->id];
                    },
                'afterCrop' => function ($model) {
                        $model->cropped = '1';
                        $model->save();
                        $response = Yii::$app->response;
                        $response->getHeaders()->set('Vary', 'Accept');
                        $response->format = Response::FORMAT_JSON;

                        return ['status' => 'success'];
                    },
            ],
            'uploadImage' => [
                'class' => \kurill\modelimages\actions\UploadImageAction::className(),
                'instanceClass' => \backend\models\TestM::className(),  //Обязательный параметр
                //'instanceName' => 'image',
            ],
            'DeleteImage' => [
                'class' => \kurill\modelimages\actions\DeleteImageAction::className(),
                'modelClass' => \kurill\modelimages\models\Image::className(),
                //'redirectUrl' => "string|Closure",
            ],
        ];
    }



