<?php
namespace kurill\modelimages\actions;
use yii\helpers\BaseFileHelper;
use Closure;
use yii\web\Response;
use Yii;

class DeleteImageAction extends \yii\base\Action{
    /** @var string ClassName of AR model */
    public $modelClass;
    /** @var Closure|array|string Closure function to get redirect url on after delete image */
    public $redirectUrl;
    
    public function run(){
        /* @var $model ActiveRecord|ImageUploaderBehavior */
        $model = new $this->modelClass;
        
        $pk = $model->getTableSchema()->primaryKey;
        $attributes = [];
        // forming search condition
        foreach ($pk as $primaryKey) {
            $pkValue = Yii::$app->request->get($primaryKey);
            if ($pkValue === null) {
                throw new InvalidParamException('You must specify "' . $primaryKey . '" param');
            }
            $attributes[$primaryKey] = $pkValue;
        }
        $model = $model->find()->where($attributes)->one();
        $subDir = $model->subDur;

        $dirToRemove = Yii::$app->getModule('yii2images')->getStorePath().DIRECTORY_SEPARATOR.$subDir;

        if(!$model->delete()){
            throw new \yii\web\ForbiddenHttpException('Image can not be deleted');
        }
        if(preg_match('/'.preg_quote($model->modelName, '/').'/', $dirToRemove)){
            BaseFileHelper::removeDirectory($dirToRemove);
        }
        
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['status' => 'success', 'message' => 'Image deleted'];
        } 
        $response = Yii::$app->response;
        $url = $this->redirectUrl instanceof Closure ? call_user_func($this->redirectUrl, $model) : $this->redirectUrl;

        return $response->redirect($url);
    }
}


        