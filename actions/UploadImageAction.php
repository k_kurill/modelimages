<?php
namespace kurill\modelimages\actions;

use yii\base\InvalidParamException;
use yii\web\UploadedFile;
use yii\helpers\Json;

class UploadImageAction extends \yii\base\Action{
    /** @var string ClassName of AR model */
    public $instanceClass;
    
    public $instanceName = 'image';
            
    public function run(){
        /* @var $model ActiveRecord|ImageUploaderBehavior */
        $model = new $this->instanceClass;
        $pk = $model->getTableSchema()->primaryKey;
        $attributes = [];
        // forming search condition
        foreach ($pk as $primaryKey) {
            $pkValue = \Yii::$app->request->get($primaryKey);
            if ($pkValue === null) {
                throw new InvalidParamException('You must specify "' . $primaryKey . '" param');
            }
            $attributes[$primaryKey] = $pkValue;
        }
        $model = $model->find()->where($attributes)->one();
        
        if(!isset($model->imageRoles[\Yii::$app->request->post('role')])){
            throw new InvalidParamException('You must specify role param');
        }
        
        $imageFile = UploadedFile::getInstanceByName($this->instanceName);
        $directory = \Yii::getAlias('@frontend/web/images');
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . '/' . $fileName;
            
            //$filePath = str_replace('\\', '/', $filePath);
            if ($imageFile->saveAs($filePath)) {
                $path = $filePath;
                $image = $model->attachImage($filePath);
                $image->role = \Yii::$app->request->post('role');
                $image->save();
                $oldPath = $image->getPathToOrigin();
                
                copy($oldPath, \demi\image\ImageUploaderBehavior::addPostfixToFile($image->getPathToOrigin(), "_original"));
                unlink($path);
                return Json::encode([
                    'files' => [[
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        "url" => $path,
                        "thumbnailUrl" => $path,
                        "deleteUrl" => 'image-delete?name=' . $fileName,
                        "deleteType" => "POST"
                    ]]
                ]);
            }
        }
        return Json::encode([
            'error' => 'File not uloaded',
        ]);
    }
}
