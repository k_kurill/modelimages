<?php
/**
 * Created by PhpStorm.
 * User: kostanevazno
 * Date: 22.06.14
 * Time: 16:58
 */

namespace kurill\modelimages\behaviors;

use rico\yii2images\behaviors\ImageBehave as BaseImageBehave;
use kurill\modelimages\models\Image;

class ImageBehave extends BaseImageBehave {
    public $roles = [];
    
    public function getImageRoles(){
        return $this->roles;
    }
    
    /**
     * returns model image by role name
     * @return array|null|ActiveRecord
     */
    public function getImageByRole($roleName){
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $finder = $this->getImagesFinder(['role'=>$roleName]);
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if(!$img){
            return $this->getModule()->getPlaceHolder();
        }
        return $img;
    }
    
    /**
     * returns array of images by role name
     * @return array|null
     */
    public function getImagesByRole($roleName){
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $finder = $this->getImagesFinder(['role'=>$roleName]);
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->all();
        if(!$img){
            return [];
        }

        return $img;
    }
    
    public function getCountImagesByRole($roleName){
        if ($this->getModule()->className === null) {
            $imageQuery = Image::find();
        } else {
            $class = $this->getModule()->className;
            $imageQuery = $class::find();
        }
        $finder = $this->getImagesFinder(['role'=>$roleName]);
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        return $imageQuery->count();
    }
    
    public function canUploadImage($roleName){
        if(!isset($this->roles[$roleName]['maxCount'])){
            return true;
        }elseif($this->roles[$roleName]['maxCount'] > $this->getCountImagesByRole($roleName)){
            return true;
        }
        return false;
    }
    
    public function maxUploadCount($roleName){
        if(!isset($this->roles[$roleName]['maxCount'])){
            return true;
        }
        return $this->roles[$roleName]['maxCount'] - $this->getCountImagesByRole($roleName);
    }
    
    private function getImagesFinder($additionWhere = false)
    {
        $base = [
            'itemId' => $this->owner->primaryKey,
            'modelName' => $this->getModule()->getShortClass($this->owner)
        ];

        if ($additionWhere) {
            $base = \yii\helpers\BaseArrayHelper::merge($base, $additionWhere);
        }

        return $base;
    }
    
}
