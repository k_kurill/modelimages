<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\web\JsExpression;
use demi\cropper\Cropper;
use demi\image\ImageUploaderBehavior;


$id = 'image_'.$image->id;
$aspectRatio = isset($model->imageRoles[$this->context->role]['aspectRatio'])?$model->imageRoles[$this->context->role]['aspectRatio']:'NaN';

$minCropBoxWidth = isset($model->imageRoles[$this->context->role]['minWidth'])?$model->imageRoles[$this->context->role]['minWidth']:'0';
$minCropBoxHeight = isset($model->imageRoles[$this->context->role]['minHeight'])?$model->imageRoles[$this->context->role]['minHeight']:'0';

$deleteBtn = Html::a('Delete <i class="glyphicon glyphicon-trash"></i>', '#',[
            'onclick' => new JsExpression('
                        if (!confirm("'.Yii::t('app','Are you realy wont delete image?').'")) {
                            return false;
                        }

                        $.ajax({
                            type: "post",
                            cache: false,
                            url: "' . Url::to(['DeleteImage', 'id' => $image->id]) . '",
                            success: function() {
                                $("#' . $id . '").remove();
                            }
                        });
                        return false;
                    '),
            'class' => 'btn btn-danger',
        ]);

$cropBtn = Cropper::widget([
    'modal' => true,
    'cropUrl' => Url::to(['cropImage', 'id' => $image->id]),
    'image' => ImageUploaderBehavior::addPostfixToFile($image->getImageSrc(), '_original'),
    'aspectRatio' => $aspectRatio,
    'pluginOptions' => ['minCropBoxWidth'=>$minCropBoxWidth, 'minCropBoxHeight'=>$minCropBoxHeight],
    'options' => ['style' => 'border-top-right-radius: 0px;border-bottom-right-radius: 0px;'],
    'ajaxOptions' => [
        'success' => new JsExpression(<<<JS
function(data) {
    // Refresh image src value to show new cropped image
    var img = $("#$id img.uploaded-image-preview");
    img.attr("src", img.attr("src").replace(/\?.*/, '') + "?" + new Date().getTime());
    img.parents('.file-preview-frame').css('box-shadow', '1px 1px 5px 0 #a2958a');
}
JS
                ),
            ],
        ]);

$editBtn = Html::a(Yii::t('app', 'Edit'), Url::to(['update-image', 'id'=>$image->id]),['class'=>'btn btn-info', 'style' => 'border-top-right-radius: 3px;border-bottom-right-radius: 3px;']);
?>
<div class="col-md-4">
    <div class="form-group file-preview-frame" style="width: 100%;">
        <div id="<?=$id?>" class="row">
            <div class="col-md-12" style="height: 240px;">
                <?= Html::img($image->imageSrc, ['class' => 'uploaded-image-preview', 'style'=>'max-width:260px;max-height:200px;']);?>
                <div class="btn-group pull-left"  style="position: absolute; bottom: 0px; width: 200px; left: 23%;" role="group">
                    <?=$deleteBtn?><?=$cropBtn?><?=$editBtn?>
                </div>
            </div>
        </div>
    </div>
</div>
