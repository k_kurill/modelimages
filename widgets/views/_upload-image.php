<?php
use \kartik\file\FileInput;
use yii\helpers\Url;

echo FileInput::widget([
        'name' => $this->context->name,
        'options'=>[
            'multiple' => $model->imageRoles[$this->context->role]['maxCount']>1,
            'accept' => 'image/*',
        ],    
        'pluginEvents' => [
            //"fileuploaded" => "function() { location.reload(); }",
        ],
        'pluginOptions' => array_merge($this->context->pluginOptions, [
            'uploadUrl' => Url::to(['uploadImage', 'id' => $model->id]),
            'uploadExtraData' => [
                'role'=>$this->context->role,
            ],
            'maxFileCount' => $model->maxUploadCount($this->context->role),
        ])
]);  
