<?php
use yii\bootstrap\Alert;
use yii\bootstrap\Html;

if(empty($this->context->role)){
    echo Alert::widget([
      'options' => [
          'class' => 'alert-error',
      ],
      'body' => 'No roles for images',
  ]);
  return;
}

echo $this->render('_upload-image', ['model' => $model]);

foreach ($model->getImagesByRole($this->context->role) as $image){
    echo $this->render('_image', ['config' => $model->imageRoles[$this->context->role], 'model' => $model, 'image' => $image]);
}
?>
<div class="clearfix"></div>